<html>
<head> 
<LINK href="pagevideo.css" rel="stylesheet" type="text/css">
<title>Benvingut</title> 
</head> 
<body>
<?php
//Autor: Carlos Mateos Moreno
//INNER JOIN EXEMPE select * from pelicula p inner join genere g on p.codgen = g.codgen and lower(genere) = 'historica';
    $con = pg_connect("host=localhost dbname=videoclub user=videoclub password=videoclub")
    or die("No s'ha pogut connectar: " . pg_last_error());
    session_start();
    include 'functions.php';
        if (isset($_SESSION["user"])){
?>
<div class ="top">
    <?php top(); ?>
</div>

<div class = "main" >
   <form action="<?php echo $_SERVER['PHP_SELF']?>" method="get"> 
    Mostra per generes: <select name="taula"><br>
    <?php
    //Mostrem els generes disponibles
    $consulta = "select genere from genere";
    $resultat = pg_query($consulta);
    while ($line = pg_fetch_array($resultat, null, PGSQL_ASSOC)) {
        echo "<option value=".$line['genere'].">" .$line['genere']."</option>";
    }
    ?>
    <input type="submit" name="boto"value="Enviar"> </br>
    </form>

<?php if(isset($_GET["boto"])){   

    $consulta = "select titol,foto from pelicula where codgen = ( select codgen from genere where genere = '" . $_GET["taula"] . "')";
    $resultat = pg_query($consulta) or die("No s'ha pogut realitzar la consulta: " . pg_last_error());
    ?> <form action="llogar.php"method="get" name ='film'> <?php
    // Printing results in HTML
    echo "<table>\n";
    while ($line = pg_fetch_array($resultat, null, PGSQL_ASSOC)) {
    echo "\t<tr>\n";
            $file = $line['titol'];
            echo '<img src= "Caratules/' .$line["foto"] . '" width="200" height="200"></br>';
            echo '<input type = "submit" name="film" value= "'. $file . '" ></br></br>';;
    echo "\t</tr>\n";
}   
echo "</table>\n";
echo "</form>";

// Free resultset
pg_free_result($resultat);

// Closing connection
pg_close($con);

}?>

</div>

<div class = "left">
<?php left(); ?>
</div>
<?php } else {
    header("Location:valida_user.php");
}?>
</body>
</html>
