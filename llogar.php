<html>
<head> 
<LINK href="pagevideo.css" rel="stylesheet" type="text/css">
<title>Llogar pel·licula</title> 
</head> 
<body>
<?php
//Autor: Carlos Mateos Moreno
//select * from dvd where codpeli = 1 and coddvd not in ( select coddvd from lloguer);
    include 'functions.php';
    $con = pg_connect("host=localhost dbname=videoclub user=videoclub password=videoclub")
    or die("No s'ha pogut connectar: " . pg_last_error());
    session_start();
    if (isset($_SESSION["user"])){
    if (isset($_GET["llogar"])){
        llogarDVD();
        header("Refresh:0; url=ConnexioBD.php");
    } else if ( isset($_GET["reservar"])){
        ReservarDVD();
        header("Refresh:0; url=ConnexioBD.php");
    }else {
?>
<div class ="top">
    <?php top(); ?>
</div>

<div class = "main" >
<?php
        //Esborrem registres anteriores a la cookie que ens puguin impedir el correcte funcionament
        setcookie("peli","",time()-3600);
       $peli = $_GET["film"];
       informacio($peli);
       $numeroPelis =  NumeroPelicules();
       if ( $numeroPelis < 3) {
            $array = dvdDisponible($peli);
            if (ComprovarPelicula($peli)){
                if ( !sizeof($array) ) {
                    setcookie("peli",$peli,time()+1800);
                    echo "Aquesta pel·licula no està disponible";
                    ?>
                    <form action="<?php echo $_SERVER['PHP_SELF']?>" method="get"> 
                    <input type ="submit" name ="reservar" value ="RESERVAR">
                    <?php
                    } else {
                        setcookie("peli",$array[0],time()+1800);
                        echo "Aquesta pel·licula està disponible </br> ";  
                        ?>
                        <form action="<?php echo $_SERVER['PHP_SELF']?>" method="get"> 
                        <input type ="submit" name ="llogar" value ="LlOGAR!">
                        </form>
                <?php } 
            } else {
                echo "Ja tens llogada aquesta pel·licula, prova amb una altra";
            }
        } else {
        echo " Tens " . $numeroPelis . " pel·licules, retorna alguna per poder llogar</br>";
        echo "<INPUT TYPE='button' VALUE='Retornar pel·licula' onClick=\"location.href='Retorna.php'\"></br>";
        }
    ?>
</div>

<div class = "left">
<?php left(); ?>
</div>

<?php } 
} else {
    header("Location:valida_user.php");
}?>

</body>
</html>

    