<html>
<head> 
<LINK href="pagevideo.css" rel="stylesheet" type="text/css">
<title>Benvingut</title> 
</head> 
<body>
<?php
//Autor: Carlos Mateos Moreno
    session_start();
    include 'functions.php';
    if (isset($_SESSION["user"])){
?>
<div class ="top">
    <?php top(); ?>
</div>
<div class = "main" >
<?php 
//Per controlar la paginació, treurem el numero de pel·licules que té disponible el client
    $con = pg_connect("host=localhost dbname=videoclub user=videoclub password=videoclub") or die("No s'ha pogut connectar: " . pg_last_error());
    $consulta="select count(codpeli) from pelicula where codpeli not in (select p.codpeli from pelicula p inner join dvd d on p.codpeli=d.codpeli inner join lloguer l on l.coddvd=d.coddvd and codsoci =  " . $_SESSION["cod"] ."  and datadev is null)";
    $resultat = pg_query($consulta) or die("No s'ha pogut realitzar la consulta: " . pg_last_error());
    $line = pg_fetch_array($resultat, null, PGSQL_ASSOC);
    $max = $line["count"];
//Controlem les diferents sortides a la paginació
if (isset($_POST["dreta_x"])){
    if ( $_SESSION["pp"] > $max) $_SESSION["pp"] = 1;
} else if (isset($_POST["esquerra_x"])) {
	$_SESSION["pp"] -= 6;
    if ( $_SESSION["pp"] == 0 ) $_SESSION["pp"]++;
    else if ( $_SESSION["pp"] < 0 ) {
    	$_SESSION["pp"] = $max + $_SESSION["pp"];
 	}
 } else {
 		$_SESSION["pp"] -= 3;
 		if ( $_SESSION["pp"] <= 0 )$_SESSION["pp"] =  $max + $_SESSION["pp"];
 }
FotosLlogar();
?>

</div>

<div class = "left">
    <?php left(); ?>
</div>
<?php } else {
	header("Location:valida_user.php");
}?>
</body>
</html>

